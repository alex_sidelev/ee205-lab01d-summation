///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE205 - Object Oriented Programming
// Lab 01b - Summation
//
// Usage:  summation n
//   n:  Sum the digits from _1_ to n
//
// Result:
//   The sum of the digits from _1_ to n
//
// Example:
//   $ summation _6
//   The sum of the digits from _1 to _6 is _21
//
// @author Alexander_Sidelev <asidelev@hawaii.edu>
// @date   01_16_2021 
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h> //imports standard imput output library
#include <stdlib.h> //imports general purpose library

int main(int argc, char* argv[]) {
   int n = atoi(argv[1]); //Accept an integer for n utilizing atoi fxn
   int i, sum; //declare the variables
   sum = 0; //assign initial sum-value to zero
   i = 1; //assign intial i-value to _1
   //utilizing a for-loop to find the sum from _1 to n
   for (i = 1; i <= n; ++i) {
      sum += i;
   }
   printf("The sum of the digits from 1 to %d is %d", n, sum);
      
   return 0;
}
