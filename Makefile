#Build a summation program in C

CC     = gcc
CFLAGS = -g -Wall

TARGET = summation

all: $(TARGET)

summation: summation.c
	$(CC) $(CFLAGS) -o $(TARGET) summation.c

clean:
	rm $(TARGET)

